int val_right=0;
int val_left=0;
int dis_right=0;
int dis_left=0;
int dis=0;
int led_left = 0;
int led_right = 0;
int flag=0;
enum { no_on, right_on, left_on , both_on} state=no_on;
enum {right2left, left2right , no2no} Direction=no2no;
void setup() {
   Serial.begin(9600);
   pinMode(7,OUTPUT);
   pinMode(5,OUTPUT);
   pinMode(LED_BUILTIN, OUTPUT);
 }
 void loop() {
   val_right = analogRead(0);
   val_left = analogRead(1);
   dis_right=28250/(val_right-129.5);
   dis_left=  28250/(val_left-129.5);
   delay(100);  
   if (dis_left < 100) //car is in front of us
   {
     led_left=1;
     digitalWrite(5, HIGH);
   }
   else {
     led_left=0;
     digitalWrite(5, LOW);
   }
 
   if (dis_right < 100) //car is in front of us
   {
     led_right=1;
     digitalWrite(7, HIGH);
   }
   else {
     led_right=0;
     digitalWrite(7, LOW);
   }
   if (led_right == 1 && led_left== 0 && state==no_on){
     state= right_on;
     Direction= right2left;
     Serial.println("right sensor senses obstacle");
   }
   else if (led_left == 1 && led_right== 0 && state==no_on){
     state= left_on;
     Direction=left2right;
     Serial.println("left sensor senses obstacle");
   }
      
   else if (led_right == 1 && led_left== 0 && state==left_on){
     state= right_on;
     Direction= right2left;
     Serial.println("right sensor senses obstacle");
   }
   else if (led_left == 1 && led_right== 0 && state==right_on){
     state= left_on;
     Direction=left2right;
     Serial.println("left sensor senses obstacle");
   }  
 //car is in front
 else if(led_left==1 && led_right==1 && (state==right_on || 80  state==left_on)){
   state=both_on;
   Serial.println("car is in front of the sensor");
 }
 else if( led_right==0 && led_left==0 && state== both_on){
   if(Direction == left2right){
     Serial.println("   a car passed from left to right");
     state=no_on;
      Direction=no2no;
   }
   else if (Direction == right2left ){
      Serial.println("   a car passed from right to left");
      state=no_on;
      Direction=no2no;
    }
  }
 }
