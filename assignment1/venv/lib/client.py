import socket
import sys


def TCP_transaction(TCP_PORT, TCP_IP, msg): #here is the TCP transaction section of assignment that is responsible for the main data transfer

    #setting the TCP buffer size
    BUFFER_SIZE = 1024

    #creating the TCP port
    TCP_SOCKET = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    #connecting the tcp connection and sending the massage which is got from the user
    TCP_SOCKET.connect((TCP_IP, TCP_PORT))
    TCP_SOCKET.send(msg)

    data = TCP_SOCKET.recv(BUFFER_SIZE)
    TCP_SOCKET.close()

    print("CLIENT_RCV_MSG="+str(data))

def check_arguments(server_address, n_port, req_code, msg, number_of_arguments):
    if int(number_of_arguments)==4:

        try:
            if isinstance(int(req_code), int):#request code is integer
                if isinstance(int(n_port), int):#n_port is integer
                    return True
                else:# the n_port is not an integer
                    return False
            else:
                return False #the request code is not an integer
        except:
            print("the format of n_port or the format of the req_code is not correct (integer is desired)")
            exit()
    else :
        print("the number of arguments for the client app should be 4 but it is "+str(number_of_arguments)+"now")
        exit()
####################################Begining of the Main code#################################



#getting arguments from the shell script
server_address = sys.argv[1]
n_port = sys.argv[2]
req_code = sys.argv[3]
msg = sys.argv[4]
number_of_arguments = sys.argv[5]

#checking the arguments
check_arguments(server_address, n_port, req_code, msg, number_of_arguments)

#setting the IP address and negotiation port and also the request code which is a random int
IP = server_address
UDP_PORT = n_port
request = req_code


#creating a udp socket and connecting to the server
UDP_SOCKET = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

#sending request code
UDP_SOCKET.sendto(request, ( IP , int(UDP_PORT) ))

#listening to the UDP socket to get TCP_PORT (r_port)

while 1:
    TCP_PORT, server_ack_address = UDP_SOCKET.recvfrom(1024) #server addr is the ip and the port of the server

    #sending the server that the client has accepted the connection
    UDP_SOCKET.sendto(str(TCP_PORT),server_ack_address)
    break

while True:# the client listens to get the ok message of the server
    server_ack_message, server_ack_address = UDP_SOCKET.recvfrom(1024)
    break

UDP_SOCKET.close()

#Starting the TCP transaction
TCP_transaction(int(TCP_PORT), server_address, msg)



