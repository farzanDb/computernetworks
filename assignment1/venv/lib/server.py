import sys
import socket

#function to reverse a string
def reverse(string):
    string = string[::-1]
    return string

number_of_passed_arguments = int(sys.argv[2])#receiving the number of arguments

if number_of_passed_arguments ==1: #checking the number of arguments to be one!

    #server sets the request code which is derived from shell
    request_code = sys.argv[1]

    try: #to catch if the request code is not and int a try catch is placed
        if isinstance(int(request_code), int):

            # Determining the localhost as the IP address
            UDP_IP = ""
            BUFFER_SIZE = 1024

            # Creating a UDP socket and binding it to a unallocated port and printing the port
            UDP_SOCKET = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            UDP_SOCKET.bind((UDP_IP, 0))
            print("SERVER_PORT=" + str(UDP_SOCKET.getsockname()[1]))

            # listening to the udp port
            while True:

                request, address = UDP_SOCKET.recvfrom(1024)  # address contains both the IP and the port of the client

                if request == request_code:  # if the client has sent the right request

                    # determining the port number and IP address
                    TCP_IP = UDP_IP
                    UDP_PORT = int(UDP_SOCKET.getsockname()[1])

                    # initiating the buffer size of the massages
                    BUFFER_SIZE = 1024

                    # creating the TCP socket using sock_stream and binding it to the port
                    TCP_SOCKET = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    TCP_SOCKET.bind((TCP_IP, 0))

                    TCP_PORT = TCP_SOCKET.getsockname()[1]
                    print("SERVER_TCP_PORT=" + str(TCP_PORT))

                    # replying back to the client with the TCP port and address
                    UDP_SOCKET.sendto(str(TCP_PORT), address)

                    while True:  # listen for the confirmation from the client
                        confirmation, address = UDP_SOCKET.recvfrom(1024)
                        break


                    if str(confirmation) == str(TCP_PORT):

                        # confirm the connection to the server by sending okay!
                        UDP_SOCKET.sendto(str("ok"), address)

                    else:

                        # disconfirming the connection to the server by sending no
                        UDP_SOCKET.sendto(str("no"), address)


                    # closing the UDP
                    UDP_SOCKET.close()

                    # starting to listen to the TCP connection
                    TCP_SOCKET.listen(1)

                    # accepting the incomming connection
                    TCP_connection, client_TCP_address = TCP_SOCKET.accept()

                    while True:
                        data = TCP_connection.recv(1024)
                        if not data:
                            break

                        # printing out the received data
                        print("SERVER_RCV_MSG=" + str(data))

                        # sending the reverced message back to the client
                        TCP_connection.send(str(reverse(data)))
                        break

                    break

    except: #request code has not been an integer
        print("the request code which was received is not an integer")

else:# the number of the passed arguments is not equeal to one
    print("the server is received "+str(number_of_passed_arguments)+" of arguments but just one argument is needed!")

